/* REGEX EXPRESSIONS FOR VALIDATIONS*/
// /^[a-zA-Z0-9]+([a-zA-Z0-9](_|-| )[a-zA-Z0-9])*[a-zA-Z0-9]+$/
let usernameRgex = /^[A-Za-z.]{3,30}$/;
let pinRegex = /^[0-9]{5}$/;
let mobileNumber = /^[237][0-9]{9} /;
let passowdRegex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8, 16}$/;
let emailRegex = /^[a-z0-9A-Z_]{3,}@[A-Za-z]{3,}[.]{1}[A-Za-z.]{2,6}$/;


// WINDOW EVENT
window.addEventListener("DOMContentLoaded", () => {
  validatorFunction();
  validateFieldsForLogin()
  loader.style.display = 'none'
  // atm.style.display = 'none'
  if (JSON.parse(localStorage.getItem('atm')) == 'isAtm') {
    atm.style.display = 'block'
    container.style.display = 'none'
  } else {
    atm.style.display = 'none'
    container.style.display = 'block'
  }
 
});

/* GET ALL ELEMENTS IN THE DOM */
let atm = document.getElementById('atmcontainer')
const signUpButton = document.getElementById("signUp");
const signInButton = document.getElementById("signIn");
const container = document.getElementById("container");
let sign_up_btn = document.getElementById("sign_up");
let form = document.querySelectorAll(".create_account input");
let validationMessage = document.querySelector(".validation_message");
let loader = document.querySelector('.loader-container')
/* ANIMATION FOR FORMS BY ADDING CLASSES */
signUpButton.addEventListener("click", () => {
  container.classList.add("right-panel-active");
});

signInButton.addEventListener("click", () => {
  container.classList.remove("right-panel-active");
});
sign_up_btn.addEventListener("click", (e) => {
  e.preventDefault();
  checkIfUserExist()

})
sign_up_btn.addEventListener("click", (e) => {
  e.preventDefault()
  // loader.style.display = 'flex'
  let checkUsername = document.querySelector(".username").value;
  let checkEmail = document.querySelector(".email").value;
  let checkPin = document.querySelector(".pinCode").value.trim();
  let checkConfirmPin = document.querySelector(".confirm").value.trim();
  let checkAmount = document.querySelector(".minimum-amount").value.trim();


  if (usernameRgex.test(checkUsername) &&
    emailRegex.test(checkEmail) &&
    pinRegex.test(checkPin) &&
    Number(checkAmount) >= 5000 &&
    checkConfirmPin === checkPin) {
      
    checkIfUserExist(checkUsername, checkEmail, checkPin, Number(checkAmount))
    
  } else {
    // loader.style.display = 'flex'
    notifierFunctionAuth('Incorrect Credentials Requirements', "error")
  }
  // loader.style.display = 'none'

});


/* AUTHENTICATION BEGINS HERE*/
const validatorFunction = () => {
  form.forEach((element) => {
    // VALIDATOR FOR USERNAME FIELD
    if (element.type === "text") {
      // ADDING KEYUP EVENT ON THE FIELD
      element.addEventListener("keyup", () => {
        if (usernameRgex.test(element.value.trim())) {
          displayValidatorMessage(element, null, "success", "error");
        } else {
          displayValidatorMessage(
            element,
            "username should be more than 3 characters",
            "error",
            "success"
          );
        }
      });
    }

    // VALIDATOR FOR EMAIL
    else if (element.type === "email") {
      // ADDING EVENT ON THE FIELD
      element.addEventListener("keyup", () => {
        if (emailRegex.test(element.value.trim())) {
          displayValidatorMessage(element, null, "success", "error");
        } else {
          displayValidatorMessage(
            element,
            "please enter a valid email",
            "error",
            "success"
          );
        }
      });
    }

    // PIN VALIDATOR
    else if (element.classList.contains("pinCode")) {
      element.addEventListener("keyup", () => {
        if (pinRegex.test(element.value.trim())) {
          displayValidatorMessage(element, null, "success", "error");
        } else {
          displayValidatorMessage(
            element,
            "PIN should be five numbers",
            "error",
            "success"
          );
        }
      });
    }

    // CONFIRM PIN
    else if (element.classList.contains("confirm")) {
      element.addEventListener("keyup", () => {
        if (document.querySelector(".pinCode").value === element.value.trim()) {
          displayValidatorMessage(element, null, "success", "error");
        } else {
          displayValidatorMessage(
            element,
            "please confirm your PIN code",
            "error",
            "success"
          );
        }
      });
    }

    // VALIDATOR FOR MINIMUM AMOUNT
    else if (element.classList.contains("minimum-amount")) {
      element.addEventListener("keyup", () => {
        if (Number(element.value.trim()) < 5000) {
          displayValidatorMessage(
            element,
            "amount is not valid",
            "error",
            "success"
          );
        } else {
          displayValidatorMessage(element, null, "success", "error");
        }
      });
    }
  });
};

// DISPLAY VALIDATOR MESSAGE FOR FIELDS FIELD
const displayValidatorMessage = (
  element,
  message,
  addedClass,
  removedClass
) => {
  element.parentElement.classList.add(addedClass);
  element.parentElement.classList.remove(removedClass);
  element.parentElement.querySelector(".validation_message").textContent =
    message;
};


// LOGIN VERIFICATIONS
document.getElementById("signInBtn").addEventListener("click", () => {
  let Email = document.getElementById("loginEmail").value;
  let Pin = document.getElementById("loginPin").value;
  verifyUser(Email, Pin)
});

const validateFieldsForLogin = () => {
  let Email = document.getElementById("loginEmail");
  let Pin = document.getElementById("loginPin");
  Email.addEventListener("keyup", () => {
    if (emailRegex.test(Email.value.trim())) {
      displayValidatorMessage(Email, null, "success", "error");
    } else {
      displayValidatorMessage(Email, "invalid email", "error", "success");
    }
  });

  Pin.addEventListener("keyup", () => {
    if (pinRegex.test(Pin.value.trim())) {
      displayValidatorMessage(Pin, null, "success", "error");
    } else {
      displayValidatorMessage(
        Pin,
        "Make sure your pin contains 5 numbers",
        "error",
        "success"
      );
    }
  });
};
// db.collection('users')


const notifierContainerAuth = document.getElementById('notifier');
const okBtnAuth = document.getElementById('okBtn')
const messagesAuth = document.querySelector('.message')

const notifierFunctionAuth = (message, type = "error" || "sucess") => {
  notifierContainerAuth.style.display = 'block';
  if (type == "error") {
    notifierContainerAuth.classList.add('notify')
    messagesAuth.textContent = message
  } else {
    notifierContainerAuth.classList.remove('notify')
    messagesAuth.textContent = message;
  }
}
okBtnAuth.addEventListener('click', () => {

  notifierContainerAuth.style.display = 'none';
  window.location.reload()
})



class User {
  UsersRef;
  constructor() {
    this.usersRef = db.collection('users');
  }

  async add(id, users) {
    try {
      const userStartInfo = {
        id:id,
        usersInfo: [users]
      }
      const result = await this.usersRef.doc(`${id}`).set(userStartInfo);
    } catch (error) {
      notifierFunctionAuth(`Make Sure you have Internet Connection`, "error")
    }
  }

  async getAllUsers() {
    const users = []
    try {
      const snapshot = await this.usersRef.get();
      snapshot.forEach(doc => users.push({
        id: doc.id,
        ...doc.data()
      }))
    } catch (error) {
      console.log(error);
    }
    return users;
  }

}

// CHECK IF DATABASE IF USER EXIST 
const checkIfUserExist = async (username, emailInput, pinInput, minAmount) => {
 
  const getAllUsers = await new User().getAllUsers()
  const isUserExisting = getAllUsers.filter(user => user.id === emailInput)

  if (isUserExisting === [] ) {
    // ADD USER TO DATABSE HERE
    const addUserToDb = new User().add(emailInput, {
      username: username,
      email: emailInput,
      PIN: pinInput,
      deposit: minAmount,
      balance: minAmount,
      transactionTime: constructDate(),
      transactionId: 0,
      cashout: 0,
    })

    notifierFunctionAuth(`${username}'s Account Created`, "sucess")
  } else if(isUserExisting[0].id.includes(`${emailInput}`)){
    // console.log();
    // USER ALREADY EXIST. THROW AN ERROR MESSAGE
    notifierFunctionAuth(`Account Already Exists`, "error")
  }
  
 
}

// LOGIN VERIFICATION METHOD
const verifyUser = async (emailInput, pin) => {
  const getAllUsers = await new User().getAllUsers()
  const isUserExisting = getAllUsers.filter(user => user.id === emailInput && user.usersInfo[0].PIN === pin)
 
  if (isUserExisting.length === 0) {
    // USER DOES NOT EXIST. SO LOGIN
    notifierFunctionAuth(`Please Sign up!`, "error")
  } else {
    // WELCOME BACK 
    localStorage.setItem('LOGIN', JSON.stringify(emailInput))
    localStorage.setItem('atm', JSON.stringify('isAtm'))
    atm.style.display = 'block'
    container.style.display = 'none'
    notifierFunctionAuth(`Welcome Back ${isUserExisting[isUserExisting.length - 1].usersInfo[0].username}`, "sucess")
  }

}

// CONSTRUCT DATE
const constructDate = () => {
  let hour = new Date().getHours();
  let dateConstructor = new Date().toString().split(' ').splice(0, 5).join(' ')
  hour = hour < 12 ? dateConstructor + " AM" : dateConstructor + " PM";
  return hour;
};