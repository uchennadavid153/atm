// let tableRow = document.getElementById('row')
let tableContainer = document.querySelector('.row')
window.addEventListener('DOMContentLoaded', () => {
  dataTable()
})
document.querySelector('.issueDate').textContent = `${new Date().toString().split(' ').splice(1, 3).join(' ')}`
let now = new Date().getHours() < 12 ? new Date().toString().split(' ')[4] + ' AM': new Date().toString().split(' ')[4] + ' PM'
document.querySelector('.time').textContent = `${now}`
const dataTable = async () => {
  const getAllUsers = await new User().getAllUsers()
  let user = JSON.parse(localStorage.getItem('LOGIN'))
  const currentUser = getAllUsers.filter(users => users.id === user)
  document.querySelector('.username').textContent = currentUser[0].usersInfo[0].username
  document.getElementById('email').textContent = currentUser[0].usersInfo[0].email;

  let displayRow = currentUser[0].usersInfo.map(user => {
    return ` 
   
    <tr>
    <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #ff0000;  line-height: 18px;  vertical-align: top; padding:10px 0;" class="article">${user.balance} FCFA</td>
    <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e;  line-height: 18px;  vertical-align: top; padding:10px 0;"><small>${user.deposit} FCFA</small></td>
    <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e;  line-height: 18px;  vertical-align: top; padding:10px 0;" align="center">${user.cashout} FCFA</td>
    <td style="font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;" align="right">${user.transactionTime.split(' ').splice(4, 5).join(' ')}</td>
    <br>
  </tr>
  	
<tr>
  <td height="0" colspan="4" style="border-bottom:1px solid #e4e4e4"></td>
</tr>

    `
  })
  displayRow = displayRow.join('')
  tableContainer.innerHTML = displayRow
}

class User {
  UsersRef;
  constructor() {
    this.usersRef = db.collection('users');
  }

  async getAllUsers() {
    const users = []
    try {
      const snapshot = await this.usersRef.get();
      snapshot.forEach(doc => users.push({
        id: doc.id,
        ...doc.data()
      }))
    } catch (error) {
      console.log(error);
    }
    return users;
  }

}