const notifierContainer = document.getElementById('notifier');
const okBtn = document.getElementById('okBtn')
const messages = document.querySelector('.message')


 const notifierFunction = (message, type = "error"|| "sucess") => {
    notifierContainer.style.display = 'block';
    if(type == "error"){
        notifierContainer.classList.add('notify')
        messages.textContent = message
    }else{
        notifierContainer.classList.remove('notify')
        messages.textContent = message;
    }
}
okBtn.addEventListener('click', ()=> {
    
    notifierContainer.style.display = 'none';
    window.location.reload()})

