let buttonsForTransaction = document.querySelectorAll('.buttonInputsContainer button')
let input = document.getElementById('AmountEntered')
let depositBtn = document.querySelector('.depositBtn')
let cashout = document.getElementById('cashout')
let atmcontainer = document.getElementById('atmcontainer')
let TransactionType = document.querySelector('.TransactionType')
let pinInput = document.getElementById('pinInput')

document.getElementById('backToMain').addEventListener('click', () => {
    atmcontainer.classList.remove('right-panel-active')
})

document.querySelectorAll('.buttonInputsContainerCheck button').forEach(btn => {
    btn.addEventListener('click', e => {
        e.preventDefault();
        if (e.target.value === undefined) {
            let values = pinInput.value.trim().split('')
            values = values.splice(0, values.length - 1).join('')
            pinInput.value = values;
            // console.log('yes');
        } else {

            pinInput.value += e.target.value
        }
    })
})

// LOGOUT EVENT HANDLER
document.querySelector('.logout').addEventListener('click', () => {
    localStorage.setItem('atm', JSON.stringify('isLogin'))
    window.location.reload()
})

// SHOW ALL BUTTONS IN THE INPUT FIELD
buttonsForTransaction.forEach(btn => {
    btn.addEventListener('click', e => {
        e.preventDefault();
        if (e.target.value === undefined) {
            let values = input.value.trim().split('')
            values = values.splice(0, values.length - 1).join('')
            input.value = values;
            // console.log('yes');
        } else {
            input.value += e.target.value
        }
    })
})

// CASHOUT EVENT HANDLER BEGINS HERE
cashout.addEventListener('click', (e) => {
    e.preventDefault();

    if (input.value == null || Number(input.value) < 100) {
        // alert('sorry impossible transaction')
        notifierFunction('Impossible Transaction', 'error')
    } else {
        // alert(`verify`)
        TransactionType.textContent = 'CASHOUT'
        atmcontainer.classList.add("right-panel-active");
    }
})

// DEPOSIT EVENT HANDLER
depositBtn.addEventListener('click', (e) => {
    e.preventDefault();
    if (Number(input.value) < 100) {
        //    alert('Please Enter An Amount Greater Than 100FCFA')
        notifierFunction('Amount Should Be Above 100 FCFA', 'error')
    } else {
        TransactionType.textContent = 'DEPOSIT'
        atmcontainer.classList.add("right-panel-active");
    }

})

document.querySelector('.confirmTransaction').addEventListener('click', (e) => {
    e.preventDefault()
    if (TransactionType.textContent === 'CASHOUT') {
        cashoutMethod();
    } else if (TransactionType.textContent === 'DEPOSIT') {
        depositMethod();
    }

})

// CASHOUT METHOD
const cashoutMethod = async () => {
    const getAllUsers = await new DepositAndCashout().getAllUsers()
    let user = JSON.parse(localStorage.getItem('LOGIN'))
    const currentUser = getAllUsers.filter(users => users.id === user)


    if (currentUser[0].usersInfo[0].PIN !== pinInput.value) {
        notifierFunction('Incorrect PIN', 'error')
    } else {
        if (Number(input.value) > currentUser[0].usersInfo[currentUser[0].usersInfo.length - 1].balance) {
            // alert('Unauthorized Action')
            notifierFunction('Unauthorized Action', 'error')
        } else {

            username = currentUser[0].usersInfo[0].username;
            email = currentUser[0].usersInfo[0].email;
            pin = currentUser[0].usersInfo[0].PIN;
            id = currentUser[0].usersInfo[currentUser[0].usersInfo.length - 1].transactionId + 1;
            time = constructNewDate();
            cashout = Number(input.value);
            deposit = 0;
            balance = currentUser[0].usersInfo[currentUser[0].usersInfo.length - 1].balance - cashout;
            let newCurrentDeposit = currentUser[0].usersInfo.concat([{
                username: username,
                email: email,
                cashout: cashout,
                PIN: pin,
                deposit: Number(deposit),
                balance: Number(balance),
                transactionTime: constructDate(),
                transactionId: id,
            }])

            const newDeposit = new DepositAndCashout().add(user, newCurrentDeposit)
            notifierFunction(`Successful Cashout of ${cashout}. Balance: ${balance}`, 'sucess')
        }
    }

}

// DEPOSIT METHOD
const depositMethod = async () => {
    const getAllUsers = await new DepositAndCashout().getAllUsers()
    let user = JSON.parse(localStorage.getItem('LOGIN'))
    const currentUser = getAllUsers.filter(users => users.id === user)

    if (currentUser[0].usersInfo[0].PIN !== pinInput.value) {
        // alert('error')
        notifierFunction('Incorrect PIN', "error")
    } else {
        username = currentUser[0].usersInfo[0].username;
        email = currentUser[0].usersInfo[0].email;
        pin = currentUser[0].usersInfo[0].PIN;
        id = currentUser[0].usersInfo[currentUser[0].usersInfo.length - 1].transactionId + 1;
        time = constructNewDate();
        cashout = 0;
        deposit = Number(input.value);
        balance = currentUser[0].usersInfo[currentUser[0].usersInfo.length - 1].balance + deposit;
        let newCurrentDeposit = currentUser[0].usersInfo.concat([{
            username: username,
            email: email,
            PIN: pin,
            deposit: Number(deposit),
            balance: Number(balance),
            transactionTime: constructDate(),
            transactionId: id,
            cashout: cashout,
        }])
        const newDeposit = new DepositAndCashout().add(user, newCurrentDeposit)
        notifierFunction(`Successful Deposit of ${Number(deposit)} FCFA`, 'sucess')
        // alert('yes')


    }

}

// CONSTRUCT DATE
const constructNewDate = () => {
    let hour = new Date().getHours();
    let dateConstructor = new Date().toString().split(' ').splice(0, 5).join(' ')
    hour = hour < 12 ? dateConstructor + " AM" : dateConstructor + " PM";
    return hour;
};


const notifierContainer = document.getElementById('notifier');
const okBtn = document.getElementById('okBtn')
const messages = document.querySelector('.message')

const notifierFunction = (message, type = "error" || "sucess") => {
    notifierContainer.style.display = 'block';
    if (type == "error") {
        notifierContainer.classList.add('notify')
        messages.textContent = message
    } else {
        notifierContainer.classList.remove('notify')
        messages.textContent = message;
    }
}
okBtn.addEventListener('click', () => {

    notifierContainer.style.display = 'none';
    window.location.reload()
})

class DepositAndCashout {
    UsersRef;
    constructor() {
        this.usersRef = db.collection('users');
    }

    async add(id, users) {
        try {
            const userStartInfo = {
                id: id,
                usersInfo: users
            }
            const result = await this.usersRef.doc(`${id}`).set(userStartInfo);
        } catch (error) {
            console.log(error);
        }
    }

    async getAllUsers() {
        const users = []
        try {
            const snapshot = await this.usersRef.get();
            snapshot.forEach(doc => users.push({
                id: doc.id,
                ...doc.data()
            }))
        } catch (error) {
            console.log(error);
        }
        return users;
    }
}