let tableRow = document.getElementById('row')
let printButton = document.getElementById('print')
window.addEventListener('DOMContentLoaded', () => {
  printButton.classList.add('removePointerEvents')
  dataTable()
  

})

const dataTable =  async () => {
  const getAllUsers = await new User().getAllUsers()
  let user = JSON.parse(localStorage.getItem('LOGIN'))
  const currentUser = getAllUsers.filter(users => users.id === user)

  
document.getElementById('username').textContent = currentUser[0].usersInfo[0].username;
document.getElementById('email').textContent = currentUser[0].usersInfo[0].email;

    // console.log();
    currentUser[0].usersInfo.map(users => {
      let tr = document.createElement('tr');
      let td1 = document.createElement('td')
      td1.innerHTML = `${users.deposit} FCFA`

      let td2 = document.createElement('td')
      td2.innerHTML = `${users.cashout} FCFA`
      let td3 = document.createElement('td')
      td3.innerHTML = `${users.balance} FCFA`
      let td4 = document.createElement('td')
      td4.innerHTML = `${users.transactionTime}`
      tr.appendChild(td1)
      tr.appendChild(td2)
      tr.appendChild(td3)
      tr.appendChild(td4)
      tableRow.appendChild(tr)
  })
  if(currentUser.length === 0){
    printButton.classList.add('removePointerEvents')

  }else{
    printButton.classList.remove('removePointerEvents')
  }
}

class User {
  UsersRef;
  constructor() {
    this.usersRef = db.collection('users');
  }

  async add(id, users) {
    try {
      const userStartInfo = {
        id:id,
        usersInfo: [users]
      }
      const result = await this.usersRef.doc(`${id}`).set(userStartInfo);
    } catch (error) {
      console.log(error);
    }
  }

  async getAllUsers() {
    const users = []
    try {
      const snapshot = await this.usersRef.get();
      snapshot.forEach(doc => users.push({
        id: doc.id,
        ...doc.data()
      }))
    } catch (error) {
      console.log(error);
    }
    return users;
  }

}